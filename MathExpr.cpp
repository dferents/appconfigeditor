// Source: http://www.daniweb.com/software-development/cpp/code/427500/calculator-using-shunting-yard-algorithm#
// Source2: https://github.com/bamos/cpp-expression-parser
// Author: Jesse Brown
// Modifications: Brandon Amos
// Modifications: www.K3A.me (changed token class, float numbers, added support for boolean operators, added support for strings)

#include <cstdlib>
#include <iostream>
#include <sstream>
#include <stdexcept>
#include <math.h>
#include "fnmatch.h"
#include <stdio.h> # sprintf

#include <ctype.h>
#include <string.h>

#include "MathExpr.h"

float MathExpr::Value::toNumber()
{
    if (isToken()) return 0;
    if (isNumber()) return number;

    number = atof(string.c_str());
    type |= NUMBER;

    return number;
}
std::string MathExpr::Value::toString()
{
    if (isToken()) return string;
    if (isString()) return string;

    char str[16];
    sprintf(str, "%f", number);
    string = str;
    type |= STRING;

    return string;
}


MathExpr::MathExpr()
{
    // 1. Create the operator precedence map.
    opPrecedence["("] = -10;
    opPrecedence["&&"]  = -2; opPrecedence["||"]  = -3;
    opPrecedence[">"]  = -1; opPrecedence[">="]  = -1;
    opPrecedence["<"]  = -1; opPrecedence["<="]  = -1;
    opPrecedence["=="]  = -1; opPrecedence["!="]  = -1;
    opPrecedence["<<"] = 1; opPrecedence[">>"] = 1;
    opPrecedence["+"]  = 2; opPrecedence["-"]  = 2;
    opPrecedence["*"]  = 3; opPrecedence["/"]  = 3;
    opPrecedence["^"] = 4;
    opPrecedence["!"] = 5;
}

#define isvariablechar(c) (isalpha(c) || c == '_')
MathExpr::ValuePtrQueue MathExpr::toRPN(const char* expr, ValueMap* vars, IntMap opPrecedence)
{
    ValuePtrQueue rpnQueue; std::stack<std::string> operatorStack;
    bool lastTokenWasOp = true;

    // In one pass, ignore whitespace and parse the expression into RPN
    // using Dijkstra's Shunting-yard algorithm.
    while (*expr && isspace(*expr)) ++expr;
    while (*expr)
    {
        if (isdigit(*expr ))
        {
            // If the token is a number, add it to the output queue.
            char* nextChar = 0;
            float digit = strtod(expr, &nextChar);

            rpnQueue.push(new Value(digit));
            expr = nextChar;
            lastTokenWasOp = false;
        }
        else if (isvariablechar(*expr ))
        {
            // If the function is a variable, resolve it and
            // add the parsed number to the output queue.
            if (!vars)
                throw std::domain_error("Detected variable, but the variable map is null.");

            std::stringstream ss;
            ss << *expr;
            ++expr;
            while (isvariablechar(*expr))
            {
                ss << *expr;
                ++expr;
            }

            std::string key = ss.str();
            if (key.compare("true") == 0)
                rpnQueue.push(new Value(1));
            else if (key.compare("false") == 0)
                rpnQueue.push(new Value(0));
            else {
                ValueMap::iterator it = vars->find(key);
                if (it == vars->end())
                    throw std::domain_error("Unable to find the variable '" + key + "'.");

                rpnQueue.push(new Value(it->second));
            }

            lastTokenWasOp = false;
        }
        else if (*expr == '\'' || *expr == '"')
        {
            // It's a string value

            char startChr = *expr;

            std::stringstream ss;
            ++expr;
            while (*expr && *expr != startChr)
            {
                ss << *expr;
                ++expr;
            }
            if (*expr) expr++;

            rpnQueue.push(new Value(ss.str()));
            lastTokenWasOp = false;
        }
        else
        {
            // Otherwise, the variable is an operator or paranthesis.
            switch (*expr) {
                case '(':
                    operatorStack.push("(");
                    ++expr;
                    break;
                case ')':
                    while (operatorStack.top().compare("(")) {
                        rpnQueue.push(new Value(operatorStack.top(), TOKEN));
                        operatorStack.pop();
                    }
                    operatorStack.pop();
                    ++expr;
                    break;
                default:
                {
                    // The token is an operator.
                    //
                    // Let p(o) denote the precedence of an operator o.
                    //
                    // If the token is an operator, o1, then
                    //   While there is an operator token, o2, at the top
                    //       and p(o1) <= p(o2), then
                    //     pop o2 off the stack onto the output queue.
                    //   Push o1 on the stack.
                    std::stringstream ss;
                    ss << *expr;
                    ++expr;
                    while (*expr && !isspace(*expr ) && !isdigit(*expr )
                           && !isvariablechar(*expr) && *expr != '(' && *expr != ')') {
                        ss << *expr;
                        ++expr;
                    }
                    ss.clear();
                    std::string str;
                    ss >> str;

                    if (lastTokenWasOp) {
                        // Convert unary operators to binary in the RPN.
                        if (!str.compare("-") || !str.compare("+") || !str.compare("!"))
                            rpnQueue.push(new Value(0));
                        else
                            throw std::domain_error("Unrecognized unary operator: '" + str + "'");

                    }

                    while (!operatorStack.empty() && opPrecedence[str] <= opPrecedence[operatorStack.top()])
                    {
                        rpnQueue.push(new Value(operatorStack.top(), TOKEN));
                        operatorStack.pop();
                    }
                    operatorStack.push(str);
                    lastTokenWasOp = true;
                }
            }
        }
        while (*expr && isspace(*expr )) ++expr;
    }
    while (!operatorStack.empty()) {
        rpnQueue.push(new Value(operatorStack.top(), TOKEN));
        operatorStack.pop();
    }
    return rpnQueue;
}


#define	EOS	'\0'

static const char *rangematch(const char *, char, int);

int fnmatch(const char *pattern, const char *string, int flags)
{
	const char *stringstart;
	char c, test;

	for (stringstart = string;;)
		switch (c = *pattern++) {
		case EOS:
			if ((flags & FNM_LEADING_DIR) && *string == '/')
				return (0);
			return (*string == EOS ? 0 : FNM_NOMATCH);
		case '?':
			if (*string == EOS)
				return (FNM_NOMATCH);
			if (*string == '/' && (flags & FNM_PATHNAME))
				return (FNM_NOMATCH);
			if (*string == '.' && (flags & FNM_PERIOD) &&
			    (string == stringstart ||
			    ((flags & FNM_PATHNAME) && *(string - 1) == '/')))
				return (FNM_NOMATCH);
			++string;
			break;
		case '*':
			c = *pattern;
			/* Collapse multiple stars. */
			while (c == '*')
				c = *++pattern;

			if (*string == '.' && (flags & FNM_PERIOD) &&
			    (string == stringstart ||
			    ((flags & FNM_PATHNAME) && *(string - 1) == '/')))
				return (FNM_NOMATCH);

			/* Optimize for pattern with * at end or before /. */
			if (c == EOS)
				if (flags & FNM_PATHNAME)
					return ((flags & FNM_LEADING_DIR) ||
					    strchr(string, '/') == NULL ?
					    0 : FNM_NOMATCH);
				else
					return (0);
			else if (c == '/' && flags & FNM_PATHNAME) {
				if ((string = strchr(string, '/')) == NULL)
					return (FNM_NOMATCH);
				break;
			}

			/* General case, use recursion. */
			while ((test = *string) != EOS) {
				if (!fnmatch(pattern, string, flags & ~FNM_PERIOD))
					return (0);
				if (test == '/' && flags & FNM_PATHNAME)
					break;
				++string;
			}
			return (FNM_NOMATCH);
		case '[':
			if (*string == EOS)
				return (FNM_NOMATCH);
			if (*string == '/' && flags & FNM_PATHNAME)
				return (FNM_NOMATCH);
			if ((pattern =
			    rangematch(pattern, *string, flags)) == NULL)
				return (FNM_NOMATCH);
			++string;
			break;
		case '\\':
			if (!(flags & FNM_NOESCAPE)) {
				if ((c = *pattern++) == EOS) {
					c = '\\';
					--pattern;
				}
			}
			/* FALLTHROUGH */
		default:
			if (c == *string)
				;
			else if ((flags & FNM_CASEFOLD) &&
				 (tolower((unsigned char)c) ==
				  tolower((unsigned char)*string)))
				;
			else if ((flags & FNM_PREFIX_DIRS) && *string == EOS &&
			     ((c == '/' && string != stringstart) ||
			     (string == stringstart+1 && *stringstart == '/')))
				return (0);
			else
				return (FNM_NOMATCH);
			string++;
			break;
		}
	/* NOTREACHED */
}

static const char *
rangematch(const char *pattern, char test, int flags)
{
	int negate, ok;
	char c, c2;

	/*
	 * A bracket expression starting with an unquoted circumflex
	 * character produces unspecified results (IEEE 1003.2-1992,
	 * 3.13.2).  This implementation treats it like '!', for
	 * consistency with the regular expression syntax.
	 * J.T. Conklin (conklin@ngai.kaleida.com)
	 */
	if ( (negate = (*pattern == '!' || *pattern == '^')) )
		++pattern;

	if (flags & FNM_CASEFOLD)
		test = tolower((unsigned char)test);

	for (ok = 0; (c = *pattern++) != ']';) {
		if (c == '\\' && !(flags & FNM_NOESCAPE))
			c = *pattern++;
		if (c == EOS)
			return (NULL);

		if (flags & FNM_CASEFOLD)
			c = tolower((unsigned char)c);

		if (*pattern == '-'
		    && (c2 = *(pattern+1)) != EOS && c2 != ']') {
			pattern += 2;
			if (c2 == '\\' && !(flags & FNM_NOESCAPE))
				c2 = *pattern++;
			if (c2 == EOS)
				return (NULL);

			if (flags & FNM_CASEFOLD)
				c2 = tolower((unsigned char)c2);

			if ((unsigned char)c <= (unsigned char)test &&
			    (unsigned char)test <= (unsigned char)c2)
				ok = 1;
		} else if (c == test)
			ok = 1;
	}
	return (ok == negate ? NULL : pattern);
}


static bool widcardCompare(std::string str, std::string wc)
{
    return !fnmatch(const_cast<const char*>(wc.c_str()), 
		const_cast<const char*>(str.c_str()),
		FNM_NOESCAPE | FNM_PERIOD);
}

MathExpr::Value MathExpr::eval(const char* expr, ValueMap* vars) {

    // Convert to RPN with Dijkstra's Shunting-yard algorithm.
    ValuePtrQueue rpn = toRPN(expr, vars, opPrecedence);

    // Evaluate the expression in RPN form.
    ValueStack evaluation;

    while (!rpn.empty()) {
        Value* tok = rpn.front();
        rpn.pop();

        if (tok->isToken())
        {
            std::string str = tok->string;
            if (evaluation.size() < 2) {
                throw std::domain_error("Invalid equation.");
            }
            Value right = evaluation.top(); evaluation.pop();
            Value left  = evaluation.top(); evaluation.pop();
            if (!str.compare("+") && left.isNumber())
                evaluation.push(left.number + right.toNumber());
            if (!str.compare("+") && left.isString())
                evaluation.push(left.string + right.toString());
            else if (!str.compare("*"))
                evaluation.push(left.toNumber() * right.toNumber());
            else if (!str.compare("-"))
                evaluation.push(left.toNumber() - right.toNumber());
            else if (!str.compare("/"))
            {
                float r = right.toNumber();
                if (r == 0)
                    evaluation.push(0);
                else
                    evaluation.push(left.toNumber() / r);
            }
            else if (!str.compare("<<"))
                evaluation.push((int)left.toNumber() << (int)right.toNumber());
            else if (!str.compare("^"))
                evaluation.push(pow(left.toNumber(), right.toNumber()));
            else if (!str.compare(">>"))
                evaluation.push((int)left.toNumber() >> (int)right.toNumber());
            else if (!str.compare(">"))
                evaluation.push(left.toNumber() > right.toNumber());
            else if (!str.compare(">="))
                evaluation.push(left.toNumber() >= right.toNumber());
            else if (!str.compare("<"))
                evaluation.push(left.toNumber() < right.toNumber());
            else if (!str.compare("<="))
                evaluation.push(left.toNumber() <= right.toNumber());
            else if (!str.compare("&&"))
                evaluation.push(left.toNumber() && right.toNumber());
            else if (!str.compare("||"))
                evaluation.push(left.toNumber() || right.toNumber());
            else if (!str.compare("=="))
            {
                if (left.isNumber() && right.isNumber())
                    evaluation.push(left.number == right.number);
                else if (left.isString() && right.isString())
                    evaluation.push(left.string == right.string);
                else if (left.isString())
                    evaluation.push(left.string == right.toString());
                else
                    evaluation.push(left.toNumber() == right.toNumber());
            }
            else if (!str.compare("!="))
            {
                if (left.isNumber() && right.isNumber())
                    evaluation.push(left.number != right.number);
                else if (left.isString() && right.isString())
                    evaluation.push(left.string != right.string);
                else if (left.isString())
                    evaluation.push(left.string != right.toString());
                else
                    evaluation.push(left.toNumber() != right.toNumber());
            }
            else if (!str.compare("=~"))
                evaluation.push( widcardCompare(left.toString(), right.toString()) );
            else if (!str.compare("!~"))
                evaluation.push( !widcardCompare(left.toString(), right.toString()) );
            else if (!str.compare("!"))
                evaluation.push(!right.toNumber());
            else
                throw std::domain_error("Unknown operator: " + left.toString() + " " + str + " " + right.toString() + ".");
        }
        else if (tok->isNumber() || tok->isString())
        {
            evaluation.push(*tok);
        }
        else
        {
            throw std::domain_error("Invalid token '" + tok->toString() + "'.");
        }

        delete tok;
    }
    return evaluation.top();
}

