/***************************************************************
 * Name:      AppConfigEditorApp.h
 * Purpose:   Defines Application Class
 * Author:     ()
 * Created:   2018-11-08
 * Copyright:  ()
 * License:
 **************************************************************/

#ifndef APPCONFIGEDITORAPP_H
#define APPCONFIGEDITORAPP_H

#include <wx/app.h>

class AppConfigEditorApp : public wxApp
{
    public:
        virtual bool OnInit();
};

#endif // APPCONFIGEDITORAPP_H
