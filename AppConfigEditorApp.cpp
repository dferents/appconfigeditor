/***************************************************************
 * Name:      AppConfigEditorApp.cpp
 * Purpose:   Code for Application Class
 * Author:     ()
 * Created:   2018-11-08
 * Copyright:  ()
 * License:
 **************************************************************/

#include "AppConfigEditorApp.h"

//(*AppHeaders
#include "AppConfigEditorMain.h"
#include <wx/image.h>
//*)

IMPLEMENT_APP(AppConfigEditorApp);

bool AppConfigEditorApp::OnInit()
{
    //(*AppInitialize
    bool wxsOK = true;
    wxInitAllImageHandlers();
    if ( wxsOK )
    {
    	AppConfigEditorFrame* Frame = new AppConfigEditorFrame(0);
    	Frame->Show();
    	SetTopWindow(Frame);
    }
    //*)
    return wxsOK;

}
