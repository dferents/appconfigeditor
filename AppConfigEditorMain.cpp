/***************************************************************
 * Name:      AppConfigEditorMain.cpp
 * Purpose:   Code for Application Frame
 * Author:     ()
 * Created:   2018-11-08
 * Copyright:  ()
 * License:
 **************************************************************/

#include "AppConfigEditorMain.h"
#include <wx/msgdlg.h>

//(*InternalHeaders(AppConfigEditorFrame)
#include <wx/intl.h>
#include <wx/string.h>
//*)
#include <wx/filedlg.h>
#include "csvparser.h"
#include <fstream>

//helper functions
enum wxbuildinfoformat {
    short_f, long_f };

wxString wxbuildinfo(wxbuildinfoformat format)
{
    wxString wxbuild(wxVERSION_STRING);

    if (format == long_f )
    {
#if defined(__WXMSW__)
        wxbuild << _T("-Windows");
#elif defined(__UNIX__)
        wxbuild << _T("-Linux");
#endif

#if wxUSE_UNICODE
        wxbuild << _T("-Unicode build");
#else
        wxbuild << _T("-ANSI build");
#endif // wxUSE_UNICODE
    }

    return wxbuild;
}

//(*IdInit(AppConfigEditorFrame)
const long AppConfigEditorFrame::ID_GRID1 = wxNewId();
const long AppConfigEditorFrame::ID_MENUITEM1 = wxNewId();
const long AppConfigEditorFrame::ID_MENU_NEW = wxNewId();
const long AppConfigEditorFrame::ID_MENU_OPEN = wxNewId();
const long AppConfigEditorFrame::ID_MENU_SAVE_AS = wxNewId();
const long AppConfigEditorFrame::ID_MENU_EXIT = wxNewId();
const long AppConfigEditorFrame::ID_STATUSBAR1 = wxNewId();
//*)

BEGIN_EVENT_TABLE(AppConfigEditorFrame,wxFrame)
    //(*EventTable(AppConfigEditorFrame)
    //*)
END_EVENT_TABLE()

AppConfigEditorFrame::AppConfigEditorFrame(wxWindow* parent,wxWindowID id)
{
    //(*Initialize(AppConfigEditorFrame)
    wxMenu* Menu1;
    wxMenuBar* MenuBar1;

    Create(parent, id, wxEmptyString, wxDefaultPosition, wxDefaultSize, wxDEFAULT_FRAME_STYLE, _T("id"));
    Grid1 = new wxGrid(this, ID_GRID1, wxPoint(160,144), wxSize(800,120), 0, _T("ID_GRID1"));
    Grid1->CreateGrid(0,4);
    Grid1->EnableEditing(true);
    Grid1->EnableGridLines(true);
    Grid1->SetColLabelValue(0, _("Section"));
    Grid1->SetColLabelValue(1, _("Key"));
    Grid1->SetColLabelValue(2, _("Value"));
    Grid1->SetColLabelValue(3, _("Description"));
    Grid1->SetDefaultCellFont( Grid1->GetFont() );
    Grid1->SetDefaultCellTextColour( Grid1->GetForegroundColour() );
    MenuBar1 = new wxMenuBar();
    Menu1 = new wxMenu();
    SelectRul = new wxMenuItem(Menu1, ID_MENUITEM1, _("Select Rule File..."), wxEmptyString, wxITEM_NORMAL);
    Menu1->Append(SelectRul);
    MenuItemNew = new wxMenuItem(Menu1, ID_MENU_NEW, _("New"), wxEmptyString, wxITEM_NORMAL);
    Menu1->Append(MenuItemNew);
    MenuItemOpen = new wxMenuItem(Menu1, ID_MENU_OPEN, _("Open..."), wxEmptyString, wxITEM_NORMAL);
    Menu1->Append(MenuItemOpen);
    MenuItemSaveAs = new wxMenuItem(Menu1, ID_MENU_SAVE_AS, _("Save As..."), wxEmptyString, wxITEM_NORMAL);
    Menu1->Append(MenuItemSaveAs);
    MenuItemExit = new wxMenuItem(Menu1, ID_MENU_EXIT, _("Exit"), wxEmptyString, wxITEM_NORMAL);
    Menu1->Append(MenuItemExit);
    MenuBar1->Append(Menu1, _("&File"));
    SetMenuBar(MenuBar1);
    StatusBar1 = new wxStatusBar(this, ID_STATUSBAR1, 0, _T("ID_STATUSBAR1"));
    int __wxStatusBarWidths_1[1] = { -1 };
    int __wxStatusBarStyles_1[1] = { wxSB_NORMAL };
    StatusBar1->SetFieldsCount(1,__wxStatusBarWidths_1);
    StatusBar1->SetStatusStyles(1,__wxStatusBarStyles_1);
    SetStatusBar(StatusBar1);

    Connect(ID_MENUITEM1,wxEVT_COMMAND_MENU_SELECTED,(wxObjectEventFunction)&AppConfigEditorFrame::OnMenuItemSelectRulSelected);
    Connect(ID_MENU_NEW,wxEVT_COMMAND_MENU_SELECTED,(wxObjectEventFunction)&AppConfigEditorFrame::OnMenuItemNewSelected);
    Connect(ID_MENU_OPEN,wxEVT_COMMAND_MENU_SELECTED,(wxObjectEventFunction)&AppConfigEditorFrame::OnMenuItemOpenSelected);
    Connect(ID_MENU_SAVE_AS,wxEVT_COMMAND_MENU_SELECTED,(wxObjectEventFunction)&AppConfigEditorFrame::OnMenuItemSaveAsSelected);
    Connect(ID_MENU_EXIT,wxEVT_COMMAND_MENU_SELECTED,(wxObjectEventFunction)&AppConfigEditorFrame::OnMenuItemExitSelected);
    //*)

 //   Grid1->EnableEditing (false);
    Menu1->Enable(ID_MENU_NEW, false);
    Menu1->Enable(ID_MENU_OPEN, false);
   // Menu1->Enable(ID_MENU_SAVE, false);
    Menu1->Enable(ID_MENU_SAVE_AS, false);
    Menu1->Enable(ID_MENU_EXIT, false);
    SetSize(1200, 600);
    Grid1->SetColSize(0, 120);
    Grid1->SetColSize(1, 160);
    Grid1->SetColSize(2, 200);
    Grid1->SetColSize(3, 650);
}

AppConfigEditorFrame::~AppConfigEditorFrame()
{
    //(*Destroy(AppConfigEditorFrame)
    //*)
}

void AppConfigEditorFrame::OnQuit(wxCommandEvent& event)
{
    Close();
}

void AppConfigEditorFrame::OnAbout(wxCommandEvent& event)
{
    wxString msg = wxbuildinfo(long_f);
    wxMessageBox(msg, _("Welcome to..."));
}

void AppConfigEditorFrame::OnGrid1CellLeftClick(wxGridEvent& event)
{
}

void AppConfigEditorFrame::OnMenuItemSelectRulSelected(wxCommandEvent& event)
{
    wxFileDialog openFileDialog(this, _("Open Config Rules file"), "", "",
                       "rul.csv (*.rul.csv)|*.rul.csv", wxFD_OPEN|wxFD_FILE_MUST_EXIST);
  if (openFileDialog.ShowModal() == wxID_CANCEL)
        return;     // the user changed idea...

    wxArrayString aFiles;
    openFileDialog.GetFilenames(aFiles);
    mRulsPath = aFiles[0];

    OnMenuItemNewSelected(event);

    wxMenu* Menu1 = GetMenuBar()->GetMenu(0);

    Menu1->Enable(ID_MENU_NEW, true);
    Menu1->Enable(ID_MENU_OPEN, true);
 //   Menu1->Enable(ID_MENU_SAVE, true);
    Menu1->Enable(ID_MENU_SAVE_AS, true);
    Menu1->Enable(ID_MENU_EXIT, true);
    Menu1->Enable(ID_MENUITEM1, false);
}

void AppConfigEditorFrame::OnMenuItemExitSelected(wxCommandEvent& event)
{
     Close();
}

void AppConfigEditorFrame::OnMenuItemSaveAsSelected(wxCommandEvent& event)
{
    if (Validate("","") == false)
        return;

    wxFileDialog saveFileDialog(this, _("Save INI file"), "", "",
                       "INI files (*.ini)|*.ini", wxFD_SAVE|wxFD_OVERWRITE_PROMPT);

   if (saveFileDialog.ShowModal() == wxID_CANCEL)
        return;     // the user changed idea...

   ofstream f (saveFileDialog.GetPath());
   string prevSec;

   int i = 0 ;
    for (auto v : mRules)
    {
        if (v[0] != prevSec)
        {
            f << "[" << v[0] << "]" << "\r\n\r\n";
            prevSec = v[0];
        }

        // description as remark
        if (v[5].size())
            f << "# " << v[5] << "\r\n";
        string val = Grid1->GetCellValue(i, 2).mb_str().data() ;
  //      string val = v[3];
        if (v[2] == "Selector")
        {
            size_t pos = val.find (" - ");
            if (pos != string::npos)
                val = val.substr (0, pos);
        }
        f << v[1] << "=" << val << "\r\n\r\n";

        i++;
    }
}

void AppConfigEditorFrame::OnMenuItemNewSelected(wxCommandEvent& event)
{
    Grid1->GoToCell (0, 0);
    int i = 0;
    //                                   file, delimiter, first_line_is_header?
    CsvParser *csvparser = CsvParser_new(mRulsPath.c_str(), ",", 1);
    CsvRow *row;
    const CsvRow *header = CsvParser_getHeader(csvparser);

    mRules.reserve(30);
    mRules.resize(0);
    mValidators.resize(0);
    while ((row = CsvParser_getRow(csvparser)) )
    {
        vector<string> vec (CsvParser_getNumFields(row));
       // mRules.resize(mRules.size()+1);
        const char **rowFields = CsvParser_getFields(row);
     //   mRules[j].resize(CsvParser_getNumFields(row));
        for (i = 0 ; i < vec.size() ; i++)
            vec[i] = rowFields[i];
        CsvParser_destroy_row(row);

        if (vec[0] == "Valitaion" || mValidators.size())
            mValidators.push_back(vec);
        else
            mRules.push_back(vec);
    }
    CsvParser_destroy(csvparser);

    i = 0;

    int aRows = Grid1->GetTable ()->GetRowsCount () ;
    int diff = aRows - mRules.size();
    if (diff > 0)
        Grid1->DeleteRows(aRows, diff);
    else if (diff < 0)
        Grid1->AppendRows(diff*-1);

    for (auto v : mRules)
    {
  //      int size = v.size();
//        string dbg = v[1];
//        for (auto s : v) dbg += s;
        Grid1->SetCellValue(i, eGridSection, v[eSection]);    // section
        Grid1->SetReadOnly(i,eGridSection,true);
        Grid1->SetCellValue(i, eGridKey, v[eKey]);    // Key
        Grid1->SetReadOnly(i,eGridKey,true);
        Grid1->SetCellValue(i, eGridDesc, v[eDesc]);    // Description
        Grid1->SetReadOnly(i,eGridDesc,true);

        Grid1->SetCellValue(i, eGridVal, v[eDefault]);    // Default
        Grid1->SetReadOnly(i,eGridVal,false);
        if (v[eType] == "Text")
        {
            auto aValidateCellEditor = new ValidateCellEditor<wxGridCellTextEditor>(20);
            aValidateCellEditor->SetFrame(this);
            Grid1->SetCellEditor(i, eGridVal, aValidateCellEditor);
        }
        else if (v[eType] == "Int")
        {
            auto aValidateCellEditor = new ValidateCellEditor<wxGridCellNumberEditor>();
            aValidateCellEditor->SetFrame(this);
            Grid1->SetCellEditor(i, eGridVal, aValidateCellEditor);
        }
        else if (v[eType] == "Float")
        {
            auto aValidateCellEditor = new ValidateCellEditor<wxGridCellFloatEditor>();
            aValidateCellEditor->SetFrame(this);
            Grid1->SetCellEditor(i, eGridVal, aValidateCellEditor);
        }
        else if (v[eType] == "Selector")
        {
            vector<wxString> vec;
            for (int x = eFirstOpt; x < v.size() && v[x].size(); x+=2)
            {
                wxString s = v[x];
                if (x+1 < v.size())
                  if (v[x+1].size())
                    s+= " - " + v[x+1];
                if (v[x] == v[eDefault])
                Grid1->SetCellValue(i, eGridVal, s);
                vec.push_back(s);
            }
            auto aValidateCellEditor = new ValidateCellEditor<wxGridCellChoiceEditor> (vec.size(), vec.data());
            aValidateCellEditor->SetFrame(this);
            Grid1->SetCellEditor(i, eGridVal, aValidateCellEditor);
        }

        i++;
    }
}
#include "MathExpr.h"
#define TEST(exprs) std::cout << (expr.eval(#exprs, &vars).toNumber() == (exprs) ? "  OK" : "FAIL") << " : " << #exprs << std::endl;
#define TEST_EXPECT(exprs, expect) std::cout << (expr.eval(exprs, &vars).toNumber() == (expect) ? "  OK" : "FAIL") << " : " << exprs << std::endl;

bool AppConfigEditorFrame::Validate (int row, int col, const string& iNewVal)
{
    if (!mRules[row][eType].size())
        return true;

    string aName = mRules[row][eKey];
    return Validate (aName, iNewVal);
}
bool AppConfigEditorFrame::Validate (const string& iKey, const string& iNewVal)
{
   MathExpr expr;
   MathExpr::ValueMap vars;

   string aValidator;

   int row = 0;
    for (auto v : mRules)
    {
        string val = Grid1->GetCellValue(row, eGridVal).mb_str().data();
        // clean desc
        size_t pos = val.find(" - ");
        if (pos != string::npos)
           val = val.substr (0, pos);

        if (v[eKey] == iKey)
        {
            val = iNewVal;
            aValidator = v[eValidator];
            if (aValidator.size() == 0)
                return true;
        }

        if (v[eType] == "Int")
            vars[v[eKey]] = atoi (val.c_str());
        else if (v[eType] == "Float")
            vars[v[eKey]] = atof (val.c_str());
        else
            vars[v[eKey]] = val;

        row++;
    }

    if (iKey.size() == 0)   // if not called from key, validate the total
        aValidator = mValidators[0][1];
    if (aValidator.size() == 0) // nothing to validate
        return true;

    vector<string> aValidators2Valid;
    size_t start = 0U;
    size_t end = aValidator.find(';');
    while (end != string::npos)
    {
        string str = aValidator.substr(start, end - start);
        aValidators2Valid.push_back (str);
        start = end + 1;
        end = aValidator.find(';', start);
    }
    string str = aValidator.substr(start, aValidator.size());
    aValidators2Valid.push_back (str);

    for (auto s : aValidators2Valid)
    {
        for (auto v : mValidators)
        {
            if (v[eValidName] == s)
            {
                str = v[eValidStatment].c_str();
                auto ret = expr.eval(v[eValidStatment].c_str(), &vars);

                float f = ret.toNumber();
                if (f == 0.0)
                {
                    wxMessageDialog(this, v[eValidFailMsg]).ShowModal ();
                    return false;
                }
            }
        }
    }
   return true;
}


#include "iniparser.h"
void AppConfigEditorFrame::OnMenuItemOpenSelected(wxCommandEvent& event)
{
    wxFileDialog openFileDialog(this, _("Open Config file"), "", "",
                       ".ini (*.ini)|*.ini", wxFD_OPEN|wxFD_FILE_MUST_EXIST);
  if (openFileDialog.ShowModal() == wxID_CANCEL)
        return;     // the user changed idea...

    OnMenuItemNewSelected(event);

    dictionary* ini = iniparser_load(openFileDialog.GetPath());

   int i = 0;
    for (auto v : mRules)
    {
        string s = iniparser_getstring(ini, (v[0] + ":" + v[1]).c_str(), NULL);
        if (s != v[3])
        {
            if (v[2] == "Selector")
            {
                for (int x = 6; x < v.size() && v[x].size(); x+=2)
                {
                    if (s == v[x])
                     if (x+1 < v.size())
                      if (v[x+1].size())
                      {
                        s+= " - " + v[x+1];
                        break;
                      }
                }
            }
            Grid1->SetCellValue(i, 2, s);
        }
        i++;
    }
}
