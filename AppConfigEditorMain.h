/***************************************************************
 * Name:      AppConfigEditorMain.h
 * Purpose:   Defines Application Frame
 * Author:     ()
 * Created:   2018-11-08
 * Copyright:  ()
 * License:
 **************************************************************/

#ifndef APPCONFIGEDITORMAIN_H
#define APPCONFIGEDITORMAIN_H

//(*Headers(AppConfigEditorFrame)
#include <wx/frame.h>
#include <wx/grid.h>
#include <wx/menu.h>
#include <wx/statusbr.h>
//*)

#include <vector>
#include <string>
using namespace std;

enum eRulColumns {eSection = 0, eKey = 1, eType = 2, eDefault = 3, eValidator = 4, eDesc = 5, eFirstOpt = 6};
enum eGridColumns {eGridSection = 0, eGridKey = 1, eGridVal = 2, eGridDesc = 3};
enum eValidatorColumns {eValidName = 0, eValidStatment = 1, eValidFailMsg = 2};

class AppConfigEditorFrame: public wxFrame
{
    string mRulsPath;
    vector<vector<string> > mRules;
    vector<vector<string> > mValidators;
    public:

        AppConfigEditorFrame(wxWindow* parent,wxWindowID id = -1);
        virtual ~AppConfigEditorFrame();

        bool Validate (int row, int col, const string& iNewVal);
        bool Validate (const string& iKey, const string& iNewVal);

    private:

        //(*Handlers(AppConfigEditorFrame)
        void OnQuit(wxCommandEvent& event);
        void OnAbout(wxCommandEvent& event);
        void OnGrid1CellLeftClick(wxGridEvent& event);
        void OnMenuItemSelectRulSelected(wxCommandEvent& event);
        void OnGrid1LeftDown(wxMouseEvent& event);
        void OnMenuItemExitSelected(wxCommandEvent& event);
        void OnMenuItemSaveAsSelected(wxCommandEvent& event);
        void OnMenuItemNewSelected(wxCommandEvent& event);
        void OnMenuItemOpenSelected(wxCommandEvent& event);
        //*)

        //(*Identifiers(AppConfigEditorFrame)
        static const long ID_GRID1;
        static const long ID_MENUITEM1;
        static const long ID_MENU_NEW;
        static const long ID_MENU_OPEN;
        static const long ID_MENU_SAVE_AS;
        static const long ID_MENU_EXIT;
        static const long ID_STATUSBAR1;
        //*)

        //(*Declarations(AppConfigEditorFrame)
        wxGrid* Grid1;
        wxMenuItem* MenuItemExit;
        wxMenuItem* MenuItemNew;
        wxMenuItem* MenuItemOpen;
        wxMenuItem* MenuItemSaveAs;
        wxMenuItem* SelectRul;
        wxStatusBar* StatusBar1;
        //*)

        DECLARE_EVENT_TABLE()
};

template <class T>
class ValidateCellEditor : public T
{
    AppConfigEditorFrame *mFrame = nullptr;
public:
    ValidateCellEditor() : T(){}
    ValidateCellEditor(int i) : T(i){}
    ValidateCellEditor(int i,  wxString* d) : T(i, d){}
    ValidateCellEditor& SetFrame(AppConfigEditorFrame *iFrame) {mFrame = iFrame; return *this;}
    virtual bool EndEdit (int row, int col, const wxGrid *grid, const wxString &oldval, wxString *newval)
    {
        string aNew = newval->mb_str().data();
        string pre = oldval.mb_str().data();
        string post = T::GetValue().mb_str().data();
        if (mFrame->Validate(row, col, post))
            return T::EndEdit (row, col, grid, oldval, newval);
        return false;
    }
};

#endif // APPCONFIGEDITORMAIN_H
